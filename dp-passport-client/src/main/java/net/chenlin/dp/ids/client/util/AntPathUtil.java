package net.chenlin.dp.ids.client.util;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import java.util.List;

/**
 * ant 地址匹配工具类
 * @author zcl<yczclcn@163.com>
 */
public class AntPathUtil {

    private static final PathMatcher ANT_PATCHER = new AntPathMatcher();

    /**
     * 使用spring antPathMatcher实现
     * @param pattern
     * @param path
     * @return
     */
    public static boolean doMatch(String pattern, String path) {
        return ANT_PATCHER.match(pattern, path);
    }

    /**
     * 匹配路径是否符合指定格式中的一种
     * @param patternList
     * @param path
     * @return
     */
    public static boolean doMatch(List<String> patternList, String path) {
        if (null == patternList || patternList.isEmpty()) {
            return false;
        }
        for (String pattern : patternList) {
            if (doMatch(pattern, path)) {
                return true;
            }
        }
        return false;
    }

}
