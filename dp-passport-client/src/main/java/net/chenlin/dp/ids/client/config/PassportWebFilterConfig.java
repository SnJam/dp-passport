package net.chenlin.dp.ids.client.config;

import net.chenlin.dp.ids.client.filter.PassportWebAuthFilter;
import net.chenlin.dp.ids.client.manager.AuthCheckManager;
import net.chenlin.dp.ids.client.servlet.WebAuthServlet;
import net.chenlin.dp.ids.client.servlet.WebAuthStatusServlet;
import net.chenlin.dp.ids.common.constant.IdsConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * passport filter配置
 * @author zcl<yczclcn@163.com>
 */
@Configuration
@ConditionalOnProperty(prefix = "ids.client", name = "client-type", havingValue = "web", matchIfMissing = true)
public class PassportWebFilterConfig {

    @Autowired
    private PassportClientConfig clientConfig;

    @Autowired
    private AuthCheckManager authCheckManager;

    /**
     * passport web过滤器配置
     * @return
     */
    @Bean
    public FilterRegistrationBean passportWebFilterRegistration() {
        FilterRegistrationBean<PassportWebAuthFilter> passportFilter = new FilterRegistrationBean<>();
        passportFilter.setName("passportAuthFilter");
        passportFilter.setOrder(1);
        passportFilter.addUrlPatterns("/*");
        passportFilter.setFilter(new PassportWebAuthFilter(clientConfig, authCheckManager));
        return passportFilter;
    }

    /**
     * authStatus servlet注册，用于校验当前用户是否登录，支持跨域请求
     * @return
     */
    @Bean
    public ServletRegistrationBean authStatusServletRegistration() {
        return new ServletRegistrationBean<>(new WebAuthStatusServlet(clientConfig, authCheckManager), IdsConst.AUTH_STATUS_URL);
    }

    /**
     * auth servlet注册，用于web登录成功后回调
     * @return
     */
    @Bean
    public ServletRegistrationBean authServletRegistration() {
        return new ServletRegistrationBean<>(new WebAuthServlet(clientConfig, authCheckManager), IdsConst.AUTH_URL);
    }

}
