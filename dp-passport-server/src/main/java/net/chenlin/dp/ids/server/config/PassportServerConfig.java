package net.chenlin.dp.ids.server.config;

import net.chenlin.dp.ids.server.util.RedisUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * passport服务端配置
 * @author zcl<yczclcn@163.com>
 */
@Configuration
@ConfigurationProperties(prefix = PassportServerConfig.IDS_SERVER_PREFIX)
public class PassportServerConfig implements InitializingBean {

    /** 属性前缀 **/
    static final String IDS_SERVER_PREFIX = "ids.server";

    /**
     * authIdCookie名称
     */
    private String authIdCookieName;

    /**
     * authIdCookie的域
     */
    private String cookieDomain;

    /**
     * redis连接信息，host和port使用 : 分隔，多个用 , 分隔
     */
    private String redisHostAndPort;

    /**
     * 默认欢迎页
     */
    private String welcomePage;

    /**
     * getter for authIdCookieName
     * @return
     */
    public String getAuthIdCookieName() {
        return authIdCookieName;
    }

    /**
     * setter for authIdCookieName
     * @param authIdCookieName
     */
    public void setAuthIdCookieName(String authIdCookieName) {
        this.authIdCookieName = authIdCookieName;
    }

    /**
     * getter for cookieDomain
     * @return
     */
    public String getCookieDomain() {
        return cookieDomain;
    }

    /**
     * setter for cookieDomain
     * @param cookieDomain
     */
    public void setCookieDomain(String cookieDomain) {
        this.cookieDomain = cookieDomain;
    }

    /**
     * getter for redisHostAndPort
     * @return
     */
    public String getRedisHostAndPort() {
        return redisHostAndPort;
    }

    /**
     * getter for welcomePage
     * @return
     */
    public String getWelcomePage() {
        return welcomePage;
    }

    /**
     * setter for welcomePage
     * @param welcomePage
     */
    public void setWelcomePage(String welcomePage) {
        this.welcomePage = welcomePage;
    }

    /**
     * setter for redisHostAndPort
     * @param redisHostAndPort
     */
    public void setRedisHostAndPort(String redisHostAndPort) {
        this.redisHostAndPort = redisHostAndPort;
    }

    /**
     * 初始化redis
     */
    @Override
    public void afterPropertiesSet() {
        RedisUtil.init(this.redisHostAndPort);
    }
}
